-- Project: RepCheker
-- Authors: Raphael 'Shugo' Boissel (raphael.boissel@orange.fr)

checker = {}
checker["load"] =
	function (this, file, doc_type, language_type)
		filedata = ""
		fileHandle = io.open(file, "r")
		if (fileHandle == nil) then
			print ("[ERROR] impossible to open file "..file)
			return nil
		end
		for line in fileHandle:lines() do
			-- File are stored with a UNIX like newline
			filedata = filedata..line.."\n"
		end
		document = {}
		document["error"] =
			function (this, pattern, msg)
				local displayed = false
				for line in (doc_type:match(filedata, pattern)) do
					if not displayed then
						print("[ERROR]"..msg) displayed = true
					end
					print("      | "..string.gsub(line, "(\n)", " "))
				end
			end
		document["warning"] =
			function (this, pattern, msg)
				local displayed = false
				for line in (doc_type:match(filedata, pattern)) do
					if not displayed then
						print("[WARNING]"..msg) displayed = true
					end
					print("      | "..string.gsub(line, "(\n)", " "))
				end
			end
		document["check"] =
			function ()
				language_type:check(document)
			end
		return document
	end
