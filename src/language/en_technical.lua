-- Project: RepCheker
-- Authors: Raphael 'Shugo' Boissel (raphael.boissel@orange.fr)

require "./src/language/en_global"
en_technical = {}
en_technical["check"] =
	function (this, document)
		en_global:check(document)

		-- Voc
		document:error("syntaxic sugar", "syntaxic sugar -> syntactic sugar?")

		-- Plurals
		document:error("syntaxic sugars", "syntaxic sugars -> syntactic sugar?")

		document:error("types declaration", "types declaration -> type declaration?")
		document:error("type declarations lists", "type declarations lists -> type declaration lists?")
		document:error("type declarations list", "type declarations list -> type declaration list?")
		document:error("[Kk]%-[Nn]earest%-[Nn]eighbor", "K-Nearest-Neighbor -> K-Nearest-Neighbors")
		document:error("[Tt]he $n$ first", "the n first -> the n firsts?")
		document:error("[Tt]he number of component", "the number of component -> the number of components?")

		-- Verbs
		document:error("[Tt]he areas in %w+ represents", "represents -> represent?")

		-- typo
		document:error("OCR", "OCR -> O.C.R.?")
		document:error("O%.C%.R", "O.C.R -> O.C.R.?")
		document:error("data base", "data base -> database?")

		-- latex
		document:error("n", "n -> $n$?")
		document:error("k", "k -> $k$?")
		document:error("O%($n$%)", "O($n$) -> $O(n)$?")
		document:error("O%($n^2$%)", "O($n$) -> $O(n)$?")
		document:error("O%(n%)", "O(n) -> $O(n)$?")
		document:error("O%(n^2%)", "O(n) -> $O(n^2)$?")
		document:error("on the \\autoref", "on the -> in?")
		document:error("in the \\autoref", "in the -> in?")
		document:error("with the \\autoref", "with the -> in?")
		document:error("with \\autoref", "with -> in?")
		document:error("[Tt]he \\autoref%b{} presents", "The ... -> ...?")

		-- Style
		document:warning("don't", "don't -> do not?")
		document:warning("doesn't", "doesn't -> does not?")
		document:warning("isn't", "isn't -> is not?")
		document:warning("wasn't", "wasn't -> was not?")
		document:warning("hasn't", "hasn't -> has not?")
		document:warning("haven't", "haven't -> have not?")
		document:warning("[Tt]he target is to present", "the target is to present -> our objective is to present?")
		document:error("various methods to reduce the", "various methods to reduce the -> various methods for reducing?")
		document:error("[Tt]he direct conclusion of this experiment", "the direct conclusion of -> the direct conclusion from?")
	end
