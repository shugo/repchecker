-- Project: RepCheker
-- Authors: Raphael 'Shugo' Boissel (raphael.boissel@orange.fr)

en_global = {}
en_global["check"] =
	function (this, document)
		-- common mistakes
		document:error("teh", "teh -> the?")
		document:error("backgroud", "backgroud -> background?")
		document:error("recommand", "recommand -> recommend?")



		-- Verbs
		document:error("becomed", "becomed -> (became / become)?")
		document:error("breaked", "breaked -> (broke / broken)?")
		document:error("findded", "findded -> found ?")
		document:error("finded", "finded -> found ?")
		document:error("freezed", "freezed -> (froze / frozen)?")
		document:error("hited", "hited -> hit?")
		document:error("hitted", "hitted -> hit?")
		document:error("holded", "holded -> held?")
		document:error("keeped", "keeped -> kept?")
		document:error("knowed", "knowed -> (knew / known)?")
		document:error("leaded", "leaded -> led?")
		document:error("maked", "maked -> made?")
		document:error("puted", "puted -> put?")
		document:error("putted", "putted -> put?")
		document:error("readed", "readed -> red?")
		document:error("showed", "showed -> shown?")
		document:error("throwed", "throwed -> (threw / thrown)?")
		document:error("writed", "writed -> (wrote / written)?")
		document:error("writted", "writted -> (wrote / written)?")

		document:error("is able to performed", "is able to performed -> is able to perform?")

		-- plurals
		document:error("advices", "advices -> advice?")
		document:error("datas", "datas -> data?")
		document:error("informations", "informations -> information?")
		document:error("softwares", "softwares -> software?")
		document:error("performances", "performances -> performance?")

		document:error("efficients", "efficients -> efficient?")
		document:error("an elements", "an elements -> an element?")

		-- Spelling
		document:error("authentication", "authentification -> authentication?")
		document:error("boolean", "boolean -> Boolean?")
		document:error("cartesian", "carthesian -> Carthesian?")
		document:error("euclidian", "euclidian -> Euclidian?")
		document:error("gaussian", "gaussian -> Gaussian?")
		document:error("theoric", "theoric -> theoretical?")
		document:error("theoretic", "theoretic -> theoretical?")

		-- Usage
		document:error("to precise", "to precise -> to specify?")
		document:error("from the point of view", "from the point of view -> from a point of view?")
		document:error("[Ww]e recommend that", "we recommend that -> we recommend to?")

		-- typo
		document:error("%w*%s[!%?%.,;:]%w*", "no space before punctuation")
		document:error("%a*[!%?%.,;:]%a+%s", "space after punctuation")
		document:error("%.%.%.", "... -> \\ldots")
		document:error(",\\ldots", ",... -> ...")
		document:error("i%.e%s", "i.e -> i.e.")
		document:error("[Oo]n the overhand", "on the overhand -> on the other hand?")
		document:error("[Oo]n the over hand", "on the over hand -> on the other hand?")

		-- expression
		document:error("take few minutes", "take few minutes -> take a few minutes?")
		document:error("and using it as is", "as is -> as-is?")
		document:error("[Bb]y doing so", "by doing so -> in doing so?")
		document:error("[Oo]n one hand,", "on one hand -> on the one hand?")

		-- Style
		document:error("First, basic principles", "First, basic principles -> First, the basic principles?")
		document:warning("to aim at doing", "to aim at doing -> to work toward doing?")
		document:warning("this property allows", "this property allows -> this property offers?")
		document:warning("contary to", "contrary to -> unlike?")
		document:warning("a critical problem", "a critical problem -> a decisive problem?")
		document:warning("usual", "usual -> common?")
		document:warning("it is not the case", "it is not the case -> it is not so?")
		document:warning("a large amount", "a large amount -> a great number?")
		document:warning("different from", "different from -> other that?")
		document:warning("implies", "implies -> leads to?")
		document:warning("need of", "need of -> need for?")
	end
