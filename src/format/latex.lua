-- Project: RepCheker
-- Authors: Raphael 'Shugo' Boissel (raphael.boissel@orange.fr)

latex = {}
latex["match"] =
	function (this, str, pattern)
		return string.gmatch(str, "(([%w]*[%s]*[%w]*)[%s%.%{%(]+"..pattern.."[%s%.%}%)]+([%w]*[%s]*[%w]*))")
	end
latex["type"] = "latex"

-- register the doctype
if doctype == nil then doctype = {} end
doctype["latex"] = latex
