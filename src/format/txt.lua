-- Project: RepCheker
-- Authors: Raphael 'Shugo' Boissel (raphael.boissel@orange.fr)

txt = {}
txt["match"] =
	function (str, pattern)
		return string.gmatch(str, pattern)
	end

txt["type"] = "text"

-- register the doctype
if doctype == nil then doctype = {} end
doctype["text"] = txt
