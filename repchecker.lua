require "./src/format/latex"
require "./src/language/en_technical"
require "./src/checker"

file = arg[1]
if file == "" or file == nil then
	print("[ERROR] No input file")
end
typestr = arg[2]
if typestr == "" or typestr == nil then
	print("[WARNING] No file type specified : Latex will be used")
	typestr = "latex"
end

typeobj = doctype[typestr]
if typeobj == nil then
	print("[ERROR] Type "..typestr.." does not exist")
	return 1
end


document = checker:load(file, typeobj, en_technical)
if document ~= nil then
	document:check()
end
